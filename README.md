
# Addressable Bug Repro Case Project

This project shows the case that calling `Addressable.LoadScene` followed by `Addressable.LoadAsset` results in the Asset never finishing loading.

Debugging into the source code of Addressables seems to show that the root cause is not actually part of Addressables, instead the bug is in AssetBundles. This issue can be shown without Addressables usage (see "Load Scene & Asset directly via AssetBundle" below). The issue affects Addressables through `UnityEngine.ResourceManagement.ResourceProviders.BundledAssetProvider.InternalOp::Start` where the completion event from `bundle.LoadAssetAsync()` is never called. It is worth noting that the `AssetBundleRequest m_RequestOperation` has the progress of 1, but the callback is never made.

These bugs are near 100% reproducible. However, it seems slightly non-deterministic -- I'm guessing an internal race condition. Some runs have loading work fine, most of the time it does not.

This project uses on-screen text and the console to display loading state.

## How to Run this Project

After cloning this project, be sure to set up addressables properly. Addressables should already be installed (via packages), but requires explicit building of AssetBundles.

Once the project is opened, open the Addressable Group window via `Window > Asset Management > Addressables > Groups`. From the Addressables Groups window, select `Play Mode Script > Use Existing Build (requires built groups)`. Then run the build via (still on Addressables Group window) `Build > New Build > Default Build Script`.

If `SampleScene` is not open, open it. Press the play button in editor (or create some build) and use the the various buttons to test various scenarios.

If a Scene is loaded followed by an GameObject, with the async loads overlapping, the Scene will finish loading but the GameObject never will.

_**NOTE:**_ You **need** to change you `Play Mode Script`, as mentioned above. If you do not, then everything will load correctly as it is not loading from built AssetBundles at all.

## Various Buttons in the scene

For any situations that are marked with "(Bug!)", check the log where print statements show more details about the state.

Scene Screenshot Showing Various Test Cases:
![Screenshot](GameScene.png)

Scene Screenshot Showing Never-Loading Asset:
![Screenshot](GameSceneShowingBug.png)


* Load Asset Alone (No Bug)
  * This loads a single asset from a single AssetBundle. This loads perfectly fine. The text (with in the scene) shows 100%. The text in the console shows that the asset finished loading.
* Load Asset & Wait Then Load Scene (No Bug).
  * This shows that loading assets first (like the first button), then waiting for them all to finish, then loading Scenes works out perfectly fine.
* Load Scene & Wait Then Load Asset (No Bug)
  * This is similar to the previous button, but it loads the scene first.
  * This shows that it doesn't matter with regard to order; there is no bug so long as there is no loading overlap.
* Load Yet Another Asset & Load Asset (No Bug)
  * This is simply to test whether Scenes are special -- apparently, they are. `YetAnotherPrefab` is in the same Addressable Group as the `EmptyScene`, but in a separate AssetBundle (as Addressables breaks those up).
* Load directly via AssetBundle (No Bug)
  * This attempts to mimic what Addressables does behind the scenes. This is just loading the AssetBundle for the Asset, then loads the Asset from said AssetBundle. It is no surprise that no bug occurs.
* Load Scene & Load Asset (*Bug!*)
  * This loads a Scene followed by an Asset/GameObject. The Scene finishes loading, but the Asset never does.
* Load Asset & Load Scene (*Bug!*)
  * This is similar to the previous buttons, simply showing that it does not matter which order the async calls happen in. There is overlap in the async calls, which leads to the Asset never finishing.
* Load Scene & Asset directly via AssetBundle (*Bug!*)
  * This attempts to mimic what Addressables does behind the scenes. This loads respective AssetBundles, then loads an Asset and a Scene from them. The Asset never finished. **This seems to show the root cause of the problem.** The issue is not an Addressables issue, it is an AssetBundle one.
* Load Other Asset (Optional to deal With ChainOps)
  * This does not affect the bug in any way. It simply loads something, just to deal with `UnityEngine.AddressableAssets.AddressablesImpl::ShouldChainRequest`. This just shows that the bug persists whether this is among the first set of Addressables loaded or not.
  * This can be used to have more descriptive debug strings on the screen.
  * This is the only button that can be pressed followed by another button.

## Other Annoyances of Note

### Chain Ops "DownloadStatus"

For the first Addressables called, `UnityEngine.AddressableAssets.AddressablesImpl::ShouldChainRequest` is true. This results in `ChainOperation`s used and returned. `UnityEngine.ResourceManagement.ChainOperation::Progress`, unlike `UnityEngine.ResourceManagement.AsyncOperations.ProviderOperation::Progress` (or other Operation `Progress` properties), has all its logic in terms of "Download", even if all the resources are local. `UnityEngine.ResourceManagement.ChainOperation::GetDownloadStatus` is used, and returns `0` if the operation is not done for any reason. This is really annoying. This bug shows the underlying resources are complete, and all parts have `1` for progress; yet `ChainOperation` will return `0` for progress for the hell of it.

### Various Progress In Same State

Loading things in slightly different order, although running into the same bug, result in different `PercentComplete` values. As mentioned in the previous section, `ChainOperation`s always show `0` as their progress. The different buttons presented in this Scene also show different `PercentComplete` values (after using the "Load Other Asset" button). The shown values (25%, 50%) may also change depending on the specific Assets being loaded -- more specifically, depending on the number of their dependencies. The percent seems to be of the form `(n - 1 + p) / n`, where `n` is the number of dependencies and `p` is a recursive term for the last dependency `PercentComplete`.

#### Load Asset & Load Scene (1st red button)
Shows 25%.
![Screenshot](Perc25.png)

#### Load Scene & Load Asset (2nd & 3th red button)
Shows 50%.
![Screenshot](Perc50.png)

#### Above Buttons Without "Load Other Asset"

ChainOp shows 0%.
![Screenshot](Perc0.png)

## Version Info

This project uses Unity 2020.2.1f1. I have seen this bug also appear on 2019.3.11f1.

This currently uses Addressables 1.16.15. I have also seen this bug with 1.16.10.

## Tested Platforms

The mentioned bugs have been tested on, and seem to be 100% reproducible on:
 * Unity Editor (Windows)
 * Windows Standalone
 * Android APK (installed on physical device)

This has been tested on 2 different Windows machines, 1 running Windows 7, 1 running Windows 10. Both show the same bugs.

## Potential Workarounds

It seems that it only happens with loading of Scenes. If one were to load all Assets (`GameObject`s) first, then loading all Scenes, it *seems* to be ok (the reverse order seems equivalent). This is not a great workaround, as it kinda defeats the point of the async nature of loading in Addressables/Unity (admittedly not the worst, as its not completely sequentially loading assets; just sequentially loading assets between 2 categories).

I am unaware of other asset types that may cause conflicts. I would assume that Scenes are the cause of the problem, and would suggest grouping Scene assets vs non-Scene assets when loading sequentially.

Example Workaround Code:
```
private IEnumerator LoadAssetsAndScenesCoroutine()
{
  foreach (object oKey in aoAssetKeys)
  {
      oAssetLoadHandles.Add(Addressables.LoadAssetAsync<GameObject>(oKey));
  }
  // Do other non-Scene asset loading..?

  yield return new WaitUntil(() => oAssetLoadHandles.All((oHandle) => oHandle.IsDone));
  foreach (object oKey in aoSceneKeys)
  {
      oSceneLoadHandles.Add(Addressables.LoadSceneAsync(oKey));
  }
}
```

## Root Issue

The core issues can be shown in the following sample (this is the logic for the "Load Scene & Asset directly via AssetBundle (*Bug!*)" button in this project):
```
Debug.Log("Loading Scene AssetBundle.");
AssetBundle.LoadFromFileAsync("Library/com.unity.addressables/aa/Windows/StandaloneWindows64/scenes_scenes_all_8a900f270531158fb607092a8170143a.bundle").completed +=
    (oAssetBundleOp) =>
    {
        Debug.Log("Scene AssetBundle loaded");
        AssetBundleCreateRequest oReq = oAssetBundleOp as AssetBundleCreateRequest;
        if (oReq != null)
        {
            Debug.Log("Loading Scene Asset.");
            AsyncOperation oSceneOp = SceneManager.LoadSceneAsync("Assets/Scenes/EmptyScene.unity", new LoadSceneParameters() { loadSceneMode = LoadSceneMode.Single });
            oSceneOp.allowSceneActivation = false;
            this.m_oOtherHandles.Add(oSceneOp);
            oSceneOp.completed +=
                (oOp) =>
                {
                    Debug.Log("Scene Asset Loaded.");
                };
        }
    };
Debug.Log("Loading Prefab AssetBundle.");
AssetBundle.LoadFromFileAsync("Library/com.unity.addressables/aa/Windows/StandaloneWindows64/prefabs_assets_all_f3f5cf0830d34e645721f3f12be53489.bundle").completed +=
    (oAssetBundleOp) =>
    {
        Debug.Log("Prefab AssetBundle loaded");
        AssetBundleCreateRequest oReq = oAssetBundleOp as AssetBundleCreateRequest;
        if (oReq != null)
        {
            Debug.Log("Loading Prefab Asset.");
            AsyncOperation oPrefabOp = oReq.assetBundle.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
            this.m_oOtherHandles.Add(oPrefabOp);
            oPrefabOp.completed +=
                (oOp) =>
                {
                    // Never happens...
                    Debug.Log("Prefab Asset LOADED!!!!!!!");
                };
        }
    };
```

## Maybe Bad Setup?

If it is the case that this project just demonstrates some improper setup, and this amounts to user error (albeit perhaps unintuitive user error), please do let me know.

It would be great if there was an error that was posted, or documentation updated. Would love to know if there was a better work around other than "sequentially ordering async loading by asset type".
