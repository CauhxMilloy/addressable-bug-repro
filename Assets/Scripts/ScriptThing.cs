
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

public class ScriptThing : MonoBehaviour, IPointerDownHandler
{
    private void Awake()
    {
        this.m_oAssetStateText = this.transform.Find("AssetStateText").GetComponent<TextMeshPro>();
        this.m_oAssetStateText.text = "Not Loaded";

        this.m_oLoadAssetButtonGameObject = this.transform.Find("LoadAssetButton").gameObject;
        this.m_oLoadAssetThenWaitSceneButtonGameObject = this.transform.Find("LoadAssetThenWaitSceneButton").gameObject;
        this.m_oLoadAssetAndSceneButtonGameObject = this.transform.Find("LoadSceneWaitButton").gameObject;
        this.m_oLoadYetAnotherAssetButtonGameObject = this.transform.Find("LoadYetAnotherAssetButton").gameObject;
        this.m_oLoadWithAssetBundleButtonGameObject = this.transform.Find("LoadWithAssetBundleButton").gameObject;

        this.m_oLoadSceneWaitButtonGameObject = this.transform.Find("LoadAssetThenSceneButton").gameObject;
        this.m_oLoadSceneButtonGameObject = this.transform.Find("LoadSceneButton").gameObject;
        this.m_oLoadSceneAssetSameGroupButtonGameObject = this.transform.Find("LoadSceneAssetSameGroupButton").gameObject;
        this.m_oLoadSceneWithAssetBundleButtonGameObject = this.transform.Find("LoadSceneWithAssetBundleButton").gameObject;

        this.m_oLoadOtherAssetButtonGameObject = this.transform.Find("LoadOtherAssetButton").gameObject;

        this.m_oOtherHandles = new List<AsyncOperation>();
    }

    private void Update()
    {
        StringBuilder oDebugTextStrBuilder = new StringBuilder();

        if (this.m_oSceneHandle.IsValid())
        {
            oDebugTextStrBuilder.Append(String.Format("{0} - {1:P2}", this.m_oSceneHandle.DebugName, this.m_oSceneHandle.PercentComplete));
            oDebugTextStrBuilder.AppendLine();
        }

        if (this.m_oPrefabHandle.IsValid())
        {
            oDebugTextStrBuilder.Append(String.Format("{0} - {1:P2}", this.m_oPrefabHandle.DebugName, this.m_oPrefabHandle.PercentComplete));
            oDebugTextStrBuilder.AppendLine();
        }

        foreach (AsyncOperation oOp in this.m_oOtherHandles)
        {
            oDebugTextStrBuilder.Append(String.Format("{0} - {1:P2}", oOp.ToString(), oOp.progress));
            oDebugTextStrBuilder.AppendLine();
        }

        this.m_oAssetStateText.text = oDebugTextStrBuilder.ToString();
    }

    void IPointerDownHandler.OnPointerDown(PointerEventData oEventData)
    {
        if (oEventData.pointerEnter == this.m_oLoadAssetButtonGameObject)
        {
            if (!this.m_oPrefabHandle.IsValid())
            {
                this.DisableButtons();

                this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
                this.StartCoroutine(this.WaitForLoad());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadAssetThenWaitSceneButtonGameObject)
        {
            this.DisableButtons();

            if (!this.m_oPrefabHandle.IsValid())
            {
                this.StartCoroutine(this.LoadAssetAndWaitThenLoadScene());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadAssetAndSceneButtonGameObject)
        {
            this.DisableButtons();

            if (!this.m_oPrefabHandle.IsValid())
            {
                this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");

                this.m_oSceneHandle = Addressables.LoadSceneAsync(
                    "Assets/Scenes/EmptyScene.unity",
                    UnityEngine.SceneManagement.LoadSceneMode.Single,
                    activateOnLoad: false);

                this.StartCoroutine(this.WaitForLoad());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadYetAnotherAssetButtonGameObject)
        {
            if (!this.m_oPrefabHandle.IsValid())
            {
                this.DisableButtons();

                Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/YetAnotherPrefab.prefab");

                this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
                this.StartCoroutine(this.WaitForLoad());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadSceneWaitButtonGameObject)
        {
            this.DisableButtons();

            if (!this.m_oPrefabHandle.IsValid())
            {
                this.StartCoroutine(this.LoadSceneAndWaitThenLoadAsset());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadSceneButtonGameObject)
        {
            this.DisableButtons();

            if (!this.m_oPrefabHandle.IsValid())
            {
                this.m_oSceneHandle = Addressables.LoadSceneAsync(
                    "Assets/Scenes/EmptyScene.unity",
                    UnityEngine.SceneManagement.LoadSceneMode.Single,
                    activateOnLoad: false);

                this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
                this.StartCoroutine(this.WaitForLoad());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadWithAssetBundleButtonGameObject)
        {
            this.DisableButtons();

            Debug.Log("Loading Prefab AssetBundle.");
            AssetBundle.LoadFromFileAsync("Library/com.unity.addressables/aa/Windows/StandaloneWindows64/prefabs_assets_all_f3f5cf0830d34e645721f3f12be53489.bundle").completed +=
                (oAssetBundleOp) =>
                {
                    Debug.Log("Prefab AssetBundle loaded");
                    AssetBundleCreateRequest oReq = oAssetBundleOp as AssetBundleCreateRequest;
                    if (oReq != null)
                    {
                        Debug.Log("Loading Prefab Asset.");
                        AsyncOperation oPrefabOp = oReq.assetBundle.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
                        this.m_oOtherHandles.Add(oPrefabOp);
                        oPrefabOp.completed +=
                            (oPrefabOp) =>
                            {
                                Debug.Log("Prefab Asset LOADED!!!!!!!");
                            };
                    }
                };
        }
        else if (oEventData.pointerEnter == this.m_oLoadSceneAssetSameGroupButtonGameObject)
        {
            this.DisableButtons();

            if (!this.m_oPrefabHandle.IsValid())
            {
                this.m_oSceneHandle = Addressables.LoadSceneAsync(
                    "Assets/Scenes/EmptyScene.unity",
                    UnityEngine.SceneManagement.LoadSceneMode.Single,
                    activateOnLoad: false);

                this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/YetAnotherPrefab.prefab");
                this.StartCoroutine(this.WaitForLoad());
            }
            else
            {
                Debug.LogError("Asset already loaded. Please reload scene.");
            }
        }
        else if (oEventData.pointerEnter == this.m_oLoadSceneWithAssetBundleButtonGameObject)
        {
            this.DisableButtons();

            Debug.Log("Loading Scene AssetBundle.");
            AssetBundle.LoadFromFileAsync("Library/com.unity.addressables/aa/Windows/StandaloneWindows64/scenes_scenes_all_8a900f270531158fb607092a8170143a.bundle").completed +=
                (oAssetBundleOp) =>
                {
                    Debug.Log("Scene AssetBundle loaded");
                    AssetBundleCreateRequest oReq = oAssetBundleOp as AssetBundleCreateRequest;
                    if (oReq != null)
                    {
                        Debug.Log("Loading Scene Asset.");
                        AsyncOperation oSceneOp = SceneManager.LoadSceneAsync("Assets/Scenes/EmptyScene.unity", new LoadSceneParameters() { loadSceneMode = LoadSceneMode.Single });
                        oSceneOp.allowSceneActivation = false;
                        this.m_oOtherHandles.Add(oSceneOp);
                        oSceneOp.completed +=
                            (oOp) =>
                            {
                                Debug.Log("Scene Asset Loaded.");
                            };
                    }
                };
            Debug.Log("Loading Prefab AssetBundle.");
            AssetBundle.LoadFromFileAsync("Library/com.unity.addressables/aa/Windows/StandaloneWindows64/prefabs_assets_all_f3f5cf0830d34e645721f3f12be53489.bundle").completed +=
                (oAssetBundleOp) =>
                {
                    Debug.Log("Prefab AssetBundle loaded");
                    AssetBundleCreateRequest oReq = oAssetBundleOp as AssetBundleCreateRequest;
                    if (oReq != null)
                    {
                        Debug.Log("Loading Prefab Asset.");
                        AsyncOperation oPrefabOp = oReq.assetBundle.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
                        this.m_oOtherHandles.Add(oPrefabOp);
                        oPrefabOp.completed +=
                            (oOp) =>
                            {
                                // Never happens...
                                Debug.Log("Prefab Asset LOADED!!!!!!!");
                            };
                    }
                };
        }
        else if (oEventData.pointerEnter == this.m_oLoadOtherAssetButtonGameObject)
        {
            Debug.Log("Loading other Asset.");
            Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/OtherPrefab.prefab").Completed += (oHandle) => Debug.Log("Finished loading other asset.");
        }
    }

    private IEnumerator WaitForLoad()
    {
        // Not necessary.. just want to wait a frame before print statement.
        yield return null;

        Debug.Log("Waiting for prefab Asset Handle...");

        yield return new WaitUntil(() => this.m_oPrefabHandle.IsDone);

        Debug.Log("Prefab Asset Handle DONE!!!! WOO!");
    }

    private IEnumerator LoadSceneAndWaitThenLoadAsset()
    {
        if (!this.m_oPrefabHandle.IsValid())
        {
            this.DisableButtons();

            this.m_oSceneHandle = Addressables.LoadSceneAsync(
                "Assets/Scenes/EmptyScene.unity",
                UnityEngine.SceneManagement.LoadSceneMode.Single,
                activateOnLoad: false);

            yield return new WaitUntil(() => this.m_oSceneHandle.IsDone);

            this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");
            this.StartCoroutine(this.WaitForLoad());
        }
        else
        {
            Debug.LogError("Asset already loaded. Please reload scene.");
        }
    }

    private IEnumerator LoadAssetAndWaitThenLoadScene()
    {
        if (!this.m_oPrefabHandle.IsValid())
        {
            this.DisableButtons();

            this.m_oPrefabHandle = Addressables.LoadAssetAsync<GameObject>("Assets/Prefabs/CoolPrefab.prefab");

            yield return new WaitUntil(() => this.m_oPrefabHandle.IsDone);

            this.m_oSceneHandle = Addressables.LoadSceneAsync(
                "Assets/Scenes/EmptyScene.unity",
                UnityEngine.SceneManagement.LoadSceneMode.Single,
                activateOnLoad: false);
            this.StartCoroutine(this.WaitForLoad());
        }
        else
        {
            Debug.LogError("Asset already loaded. Please reload scene.");
        }
    }

    private void DisableButtons()
    {
        this.m_oLoadAssetButtonGameObject.SetActive(false);
        this.m_oLoadSceneButtonGameObject.SetActive(false);
        this.m_oLoadAssetAndSceneButtonGameObject.SetActive(false);
        this.m_oLoadSceneWaitButtonGameObject.SetActive(false);
        this.m_oLoadAssetThenWaitSceneButtonGameObject.SetActive(false);

        this.m_oLoadWithAssetBundleButtonGameObject.SetActive(false);
        this.m_oLoadSceneWithAssetBundleButtonGameObject.SetActive(false);
        this.m_oLoadSceneAssetSameGroupButtonGameObject.SetActive(false);
        this.m_oLoadYetAnotherAssetButtonGameObject.SetActive(false);

        this.m_oLoadOtherAssetButtonGameObject.SetActive(false);
    }

    private TextMeshPro m_oAssetStateText;

    private GameObject m_oLoadAssetButtonGameObject;
    private GameObject m_oLoadSceneButtonGameObject;
    private GameObject m_oLoadAssetAndSceneButtonGameObject;
    private GameObject m_oLoadSceneWaitButtonGameObject;
    private GameObject m_oLoadAssetThenWaitSceneButtonGameObject;

    private GameObject m_oLoadWithAssetBundleButtonGameObject;
    private GameObject m_oLoadSceneWithAssetBundleButtonGameObject;
    private GameObject m_oLoadSceneAssetSameGroupButtonGameObject;
    private GameObject m_oLoadYetAnotherAssetButtonGameObject;

    private GameObject m_oLoadOtherAssetButtonGameObject;

    private AsyncOperationHandle<SceneInstance> m_oSceneHandle;
    private AsyncOperationHandle<GameObject> m_oPrefabHandle;
    private List<AsyncOperation> m_oOtherHandles;
}
